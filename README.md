<h1 align="center">Welcome to san-email 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.1-blue.svg?cacheSeconds=2592000" />
  <a href="https://twitter.com/santosh8055">
    <img alt="Twitter: santosh8055" src="https://img.shields.io/twitter/follow/santosh8055.svg?style=social" target="_blank" />
  </a>
</p>

> A vue project to show your emails

### 🏠 [Homepage](https://sanemail.netlify.com/#/)

## Install

```sh
yarn
```

## Usage

```sh
yarn serve
```

## Run tests

```sh
yarn test:unit
```

### About this application

Basically, this application contains two pages, they are

- Home
- About

### Home

The home page is the main page of this application. This page contains two sections they are

- List
- Detail

Since this application is responsive, sections appear different in different screen sizes.
The second section is hidden by default in smaller screens and visible as a popup on demand.

### List section

The List section contains the list of emails in tabular format. Each row is clickable.
Once the user clicks on a row, that particular email is selected and shown in the Detail view.
All the unread emails are shown in bold, Once the email is read it is marked as read.
User can see the font weight is initial in that case.
You can see the bold rows in the screen shot

### Detail Section

Detail section contains the whole email contains details including but not limited to
senders name, email, the date on which email sent. Detail section appears different in
different screen sizes for readability. In smaller screens, this section appears as a popup.

### Routing

This application is a single page application with two pages and those pages are home
and about. Both the pages can are accessible from the header. User may click on the
'Email' text to navigate to the home page and 'About' text on the header
to navigate to about page.

# Technical details

This application is developed using Vue framework.

There are two views, About and Home. About is just a plane template but Home uses two components, they are
Detail and List.

There are three main components in this application they are Container, Detail and
List.

### Container component.

Container component contains the basic routing logic. Here we also fetch the data using API.
This component contains two child components, Detail, and List.

### List component.

v-for directive is used in this component to show the list of the emails.
All the emails are passes as props to this directive.

###Detail component

When the user clicks on a row in list component, that email will be selected and shown in the detail
component.

##Testing

Jest framework is used for testing this app.
Once you set up this application locally you can run the unit testing command from package.json file to run
all the test cases.

There are two files related to testing. You can find them in the test folder. All the test cases are basic.

## Styling

CSS is used for styling the app and the components.
Media queries are used for making the app responsive
and keyframes are used for adding animation.

### Additional libraries

No additional libraries are used in this application except Vue framework.
Although, ES6 syntax is widely used.

# Missing functionality

- Filter feature in the list.
- Animations while navigating between pages.
- Formatting date in the emails.
- Sorting emails feature.
- Make popup more appealing.

## Author

👤 **Santosh Pasupunuri**

- Twitter: [@santosh8055](https://twitter.com/santosh8055)
- Github: [@santosh8055](https://github.com/santosh8055)

## Show your support

Give a ⭐️ if this project helped you!
