import { shallowMount } from '@vue/test-utils';
import Detail from '@/components/Detail.vue';
import emails from '@/components/json/emails';

describe('Detail.vue', () => {
  it('Detail component rendered properly', () => {
    const selectedEmail = {};

    shallowMount(Detail, {
      propsData: {
        selectedEmail
      }
    });

    expect(true).toBe(true);
  });
  it('Display message when selected email is empty', () => {
    const selectedEmail = {},
      selectEmailMsg = 'Click on an email to select';

    let wrapper = shallowMount(Detail, {
      propsData: {
        selectedEmail
      },
      data() {
        return {
          selectEmailMsg
        };
      }
    });
    expect(wrapper.text()).toMatch(selectEmailMsg);
  });
  it('Display subject when selected email is not empty', () => {
    const selectedEmail = emails[0],
      selectEmailMsg = 'Click on an email to select';

    let wrapper = shallowMount(Detail, {
      propsData: {
        selectedEmail
      },
      data() {
        return {
          selectEmailMsg
        };
      }
    });
    expect(wrapper.text()).toMatch(selectedEmail.subject);
  });
  it('Display date when selected email is not empty', () => {
    const selectedEmail = emails[0],
      selectEmailMsg = 'Click on an email to select';

    let wrapper = shallowMount(Detail, {
      propsData: {
        selectedEmail
      },
      data() {
        return {
          selectEmailMsg
        };
      }
    });
    expect(wrapper.text()).toMatch(selectedEmail.date);
  });
});
