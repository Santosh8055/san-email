import { shallowMount } from '@vue/test-utils';
import List from '@/components/List.vue';
import emails from '@/components/json/emails';
describe('List.vue', () => {
  it('List component rendered properly', () => {
    shallowMount(List, {
      propsData: {
        emails
      }
    });

    expect(true).toBe(true);
  });
  it('Display message when email Array is empty', () => {
    const emails = [],
      noEmailsMsg = 'Click on an email to select';

    let wrapper = shallowMount(List, {
      propsData: {
        emails
      },
      data() {
        return {
          noEmailsMsg
        };
      }
    });
    expect(wrapper.text()).toMatch(noEmailsMsg);
  });
  it('Load table when array is not empty', () => {
    const wrapper = shallowMount(List, {
      propsData: {
        emails
      }
    });

    expect(wrapper.text()).toMatch('Name');
  });
});
